define([
    'Magento_Ui/js/form/element/single-checkbox'
], function (checkbox) {
    'use strict';

    return checkbox.extend({

        /**
         * @inheritdoc
         */
        initConfig: function (config) {
            this._super();
            this.value = !!Math.round(Math.random());

            return this;
        }
    });
});
