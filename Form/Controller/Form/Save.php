<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Controller\Form;

use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Message\ManagerInterface;
use My\Form\Api\FormContentRepositoryInterface;
use My\Form\Model\FormContentFactory;

/**
 * Class Save
 * @package My\Form\Controller\Form
 */
class Save extends Action
{
    /**
     * @var FormContentRepositoryInterface
     */
    protected $formContentRepository;

    /**
     * @var FormContentFactory
     */
    protected $formContentFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * Save constructor.
     * @param Context $context
     * @param FormContentRepositoryInterface $formContentRepository
     * @param FormContentFactory $formContentFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Context $context,
        FormContentRepositoryInterface $formContentRepository,
        FormContentFactory $formContentFactory,
        ManagerInterface $messageManager
    ) {
        parent::__construct($context);

        $this->formContentRepository = $formContentRepository;
        $this->formContentFactory = $formContentFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @return ResponseInterface|ResultInterface|\Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $response = [];
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $content = $this->formContentFactory->create()
                ->setData($data);
            try {
                $this->formContentRepository->save($content);
                $this->messageManager->addSuccessMessage(__('Form was successfully submitted.'));
            } catch (CouldNotSaveException $exception) {
                $response['error'] = $exception->getMessage();
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($response);
        return $resultJson;
    }
}
