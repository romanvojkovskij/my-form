<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Console\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use My\Form\Api\CountryResponseApiInterfaceFactory;
use My\Form\Model\Country\ImportFactory;

/**
 * Class ImportCountries
 * @package My\Form\Console\Command
 */
class ImportCountries extends Command
{
    /**
     * @var CountryResponseApiInterfaceFactory
     */
    private $apiFactory;

    /**
     * @var ImportFactory
     */
    private $importModel;

    /**
     * @var string|null
     */
    private $name;

    /**
     * ImportCountries constructor.
     *
     * @param CountryResponseApiInterfaceFactory $apiFactory
     * @param ImportFactory $importModel
     * @param string|null $name
     */
    public function __construct(
        CountryResponseApiInterfaceFactory $apiFactory,
        ImportFactory $importModel,
        string $name = null
    ) {
        parent::__construct($name);

        $this->apiFactory = $apiFactory;
        $this->importModel = $importModel;
        $this->name = $name;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('my-form:countries:import')
            ->setDescription('Import countries form API');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return false|int
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $countries = $this->apiFactory->create()->getResponse();
        if (is_null($countries)) {
            $output->writeln('<warning>We could not find any countries.</warning>');
            return false;
        }

        $this->importModel->create()
            ->import($countries);
        $output->writeln('<info>Done.</info>');

        return 0;
    }
}
