<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use My\Form\Api\Data\FormContentInterface;

/**
 * Class FormContent
 * @package My\Form\Model
 */
class FormContent extends AbstractModel implements FormContentInterface, IdentityInterface
{
    const CACHE_TAG = 'my_form_content';

    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(ResourceModel\FormContent::class);
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name): FormContent
    {
        $this->setData(self::NAME, $name);

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @param $address
     * @return $this
     */
    public function setAddress($address): FormContent
    {
        $this->setData(self::ADDRESS, $address);

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email): FormContent
    {
        $this->setData(self::EMAIL, $email);

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @param $phone
     * @return $this
     */
    public function setPhone($phone): FormContent
    {
        $this->setData(self::PHONE, $phone);

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->getData(self::PHONE);
    }

    /**
     * @param $countryId
     * @return $this
     */
    public function setCountryId($countryId): FormContent
    {
        $this->setData(self::COUNTRY_ID, $countryId);

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryId(): string
    {
        return $this->getData(self::COUNTRY_ID);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
