<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use My\Form\Api\Data\CountryInterface;
use My\Form\Api\CountryRepositoryInterface;
use My\Form\Model\ResourceModel\Country as CountryResource;

/**
 * Class CountryRepository
 * @package My\Form\Model
 */
class CountryRepository implements CountryRepositoryInterface
{
    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var CountryResource
     */
    protected $resource;

    /**
     * CountryRepository constructor.
     * @param CountryFactory $countryFactory
     * @param CountryResource $resource
     */
    public function __construct(CountryFactory $countryFactory, CountryResource $resource)
    {
        $this->resource = $resource;
        $this->countryFactory = $countryFactory;
    }

    /**
     * @param int $id
     * @return CountryInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): CountryInterface
    {
        $country = $this->countryFactory->create();
        $this->resource->load($country, $id);
        if (!$country->getId()) {
            throw new NoSuchEntityException(__('Unable to find data with ID "%1"', $id));
        }
        return $country;
    }

    /**
     * @param CountryInterface|Country $country
     * @return CountryInterface
     * @throws CouldNotSaveException
     */
    public function save(CountryInterface $country): CountryInterface
    {
        try {
            $this->resource->save($country);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the data: %1', $exception->getMessage()),
                $exception
            );
        }
        return $country;
    }

    /**
     * Get by Code value
     * @param string $value
     * @return CountryInterface
     * @throws NoSuchEntityException
     */
    public function getByCountryId(string $value): CountryInterface
    {
        $country = $this->countryFactory->create();
        $this->resource->load($country, $value, 'country_id');

        if (!$country->getId()) {
            throw new NoSuchEntityException(__('No such Country'));
        }

        return $country;
    }

    /**
     * @param CountryInterface $country
     * @return void
     */
    public function delete(CountryInterface $country)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        // TODO: Implement getList() method.
    }
}
