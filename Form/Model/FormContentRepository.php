<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use My\Form\Api\Data\FormContentInterface;
use My\Form\Api\FormContentRepositoryInterface;
use My\Form\Model\ResourceModel\FormContent as FormContentResource;

/**
 * Class FormContentRepository
 * @package My\Form\Model
 */
class FormContentRepository implements FormContentRepositoryInterface
{
    /**
     * @var FormContentFactory
     */
    protected $formContentFactory;

    /**
     * @var FormContentResource
     */
    protected $resource;

    /**
     * FormContentRepository constructor.
     * @param FormContentFactory $formContentFactory
     * @param FormContentResource $resource
     */
    public function __construct(FormContentFactory $formContentFactory, FormContentResource $resource)
    {
        $this->resource = $resource;
        $this->formContentFactory = $formContentFactory;
    }

    /**
     * @param int $id
     * @return FormContentInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): FormContentInterface
    {
        $content = $this->formContentFactory->create();
        $this->resource->load($content, $id);
        if (!$content->getId()) {
            throw new NoSuchEntityException(__('Unable to find data with ID "%1"', $id));
        }
        return $content;
    }

    /**
     * @param FormContentInterface|FormContent $formContent
     * @return FormContentInterface
     * @throws CouldNotSaveException
     */
    public function save(FormContentInterface $formContent): FormContentInterface
    {
        try {
            $this->resource->save($formContent);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the data: %1', $exception->getMessage()),
                $exception
            );
        }
        return $formContent;
    }

    /**
     * @param FormContentInterface $formContent
     * @return void
     */
    public function delete(FormContentInterface $formContent)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        // TODO: Implement getList() method.
    }
}
