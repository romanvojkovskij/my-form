<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model\ResourceModel\Country;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use My\Form\Model\Country;
use My\Form\Model\ResourceModel\Country as ResourceCountry;

class Collection extends AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init(Country::class, ResourceCountry::class);
    }
}
