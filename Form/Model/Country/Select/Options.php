<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model\Country\Select;

use My\Form\Model\Country;
use My\Form\Model\ResourceModel\Country\CollectionFactory;

/**
 * Class Options
 * @package My\Form\Model\Country\Select
 */
class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * Options constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array[] Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options = [['value' => '', 'label' => __('Country')]];
        $collection = $this->collectionFactory->create();
        $countries = $collection->getItems();
        if (count($countries)) {
            /** @var Country $country */
            foreach ($countries as $country) {
                $options[] = [
                    'value' => $country->getCountryId(),
                    'label' => $country->getName()
                ];
            }
        }

        return $options;
    }
}
