<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model\Country;

use Magento\Framework\Exception\NoSuchEntityException;
use My\Form\Api\CountryRepositoryInterface;
use My\Form\Api\Data\CountryInterface;
use My\Form\Model\Country;
use My\Form\Model\CountryFactory;
use My\Form\Model\ResourceModel\Country as CountryResource;
use Psr\Log\LoggerInterface;

/**
 * Class Import
 * @package My\Form\Model\Country
 */
class Import
{
    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Import constructor.
     * @param CountryFactory $countryFactory
     * @param CountryRepositoryInterface $countryRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        CountryFactory $countryFactory,
        CountryRepositoryInterface $countryRepository,
        LoggerInterface $logger
    ) {
        $this->countryFactory = $countryFactory;
        $this->countryRepository = $countryRepository;
        $this->logger = $logger;
    }

    /**
     * @param array $countries
     * @return false
     * @throws \Exception
     */
    public function import(array $countries)
    {
        if (!count($countries)) {
            return false;
        }

        foreach ($countries as $countryData) {
            $country = $this->getCountryModel($countryData['alpha2Code']);
            $country->setName($countryData['name']);
            $country->setCountryId($countryData['alpha2Code']);

            try {
                $this->countryRepository->save($country);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $this->logger->error($e->getTraceAsString());
            }
        }
    }

    /**
     * @param string $countryCode
     * @return CountryInterface|Country
     */
    private function getCountryModel(string $countryCode): Country
    {
        try {
            $country = $this->countryRepository->getByCountryId($countryCode);
        } catch (NoSuchEntityException $e) {
            $country = $this->countryFactory->create();
        }

        return $country;
    }
}
