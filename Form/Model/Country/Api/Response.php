<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model\Country\Api;

use Magento\Framework\Serialize\SerializerInterface;
use My\Form\Api\CountryResponseApiInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Response
 * @package My\Form\Model\Country\Api
 */
class Response implements CountryResponseApiInterface
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CountryApi constructor.
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     */
    public function __construct(Request $request, SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->request = $request;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * @return array|null
     */
    public function getResponse(): ?array
    {
        $response = $this->request->request();
        if (is_null($response)) {
            $this->logger->alert('The API request returned an empty response.');
            return null;
        }

        $countries = $this->serializer->unserialize($response);
        $this->logger->info('API response: ' . $response);
        return $countries;
    }
}
