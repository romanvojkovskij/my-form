<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Model\Country\Api;

use Psr\Log\LoggerInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\HTTP\AsyncClient\Request as AsyncClientRequest;
use My\Form\Api\CountryRequestApiInterface;


/**
 * Class Request
 * @package My\Form\Model\Country\Api
 */
class Request implements CountryRequestApiInterface
{
    /**
     * @var Curl
     */
    protected $curlClient;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CountryApi constructor.
     * @param Curl $curlClient
     * @param LoggerInterface $logger
     */
    public function __construct(Curl $curlClient, LoggerInterface $logger)
    {
        $this->curlClient = $curlClient;
        $this->logger = $logger;
    }

    /**
     * @param string $method
     * @return array|null
     */
    public function request($method = AsyncClientRequest::METHOD_GET): ?string
    {
        try {
            $this->curlClient->get(self::COUNTRIES_API_ENDPOINT);
            return $this->curlClient->getBody();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());

            return null;
        }
    }
}
