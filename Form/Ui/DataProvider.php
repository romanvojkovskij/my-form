<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Ui;

use My\Form\Model\ResourceModel\Country\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collection,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collection->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return [];
    }
}
