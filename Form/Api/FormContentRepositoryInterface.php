<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use My\Form\Api\Data\FormContentInterface;

/**
 * Interface FormContentRepositoryInterface
 * @package My\Form\Api
 */
interface FormContentRepositoryInterface
{
    /**
     * @param int $id
     * @return FormContentInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): FormContentInterface;

    /**
     * @param FormContentInterface $formContent
     * @return FormContentInterface
     * @trows CouldNotSaveException
     */
    public function save(FormContentInterface $formContent): FormContentInterface;

    /**
     * @param FormContentInterface $formContent
     * @return void
     */
    public function delete(FormContentInterface $formContent);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
