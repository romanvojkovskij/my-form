<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use My\Form\Api\Data\CountryInterface;

/**
 * Interface CountryRepositoryInterface
 * @package My\Form\Api
 */
interface CountryRepositoryInterface
{
    /**
     * @param int $id
     * @return CountryInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): CountryInterface;

    /**
     * Get by Code value
     * @param string $value
     * @return CountryInterface
     * @throws NoSuchEntityException
     */
    public function getByCountryId(string $value): CountryInterface;

    /**
     * @param CountryInterface $country
     * @return CountryInterface
     * @trows CouldNotSaveException
     */
    public function save(CountryInterface $country): CountryInterface;

    /**
     * @param CountryInterface $country
     * @return void
     */
    public function delete(CountryInterface $country);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
