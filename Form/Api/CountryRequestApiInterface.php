<?php

/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Api;

use Magento\Framework\HTTP\AsyncClient\Request;

/**
 * Interface CountryRequestApiInterface
 * @package My\Form\Api
 */
interface CountryRequestApiInterface
{
    /**
     * Of course it should be configurable in Magento admin
     * and implemented via configuration
     */
    const COUNTRIES_API_ENDPOINT = 'https://restcountries.eu/rest/v2/all';

    /**
     * @param string $method
     * @return mixed
     */
    public function request($method = Request::METHOD_GET): ?string;
}
