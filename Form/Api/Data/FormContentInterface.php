<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Api\Data;

use My\Form\Model\FormContent;

/**
 * Interface FormContentInterface
 * @package My\Form\Api\Data
 */
interface FormContentInterface
{
    const NAME = 'name';
    const ADDRESS = 'address';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const COUNTRY_ID = 'country_id';

    /**
     * @param $name
     * @return FormContent
     */
    public function setName($name): FormContent;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param $address
     * @return FormContent
     */
    public function setAddress($address): FormContent;

    /**
     * @return string
     */
    public function getAddress(): string;

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email): FormContent;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @param $phone
     * @return FormContent
     */
    public function setPhone($phone): FormContent;

    /**
     * @return string
     */
    public function getPhone(): string;

    /**
     * @param $countryId
     * @return FormContent
     */
    public function setCountryId($countryId): FormContent;

    /**
     * @return string
     */
    public function getCountryId(): string;
}
