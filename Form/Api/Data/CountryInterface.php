<?php
/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Api\Data;

interface CountryInterface
{
    const ID = 'id';
    const NAME = 'name';
    const COUNTRY_ID = 'country_id';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getCountryId(): string;

    /**
     * @param $countryId
     * @return $this
     */
    public function setCountryId($countryId);
}
