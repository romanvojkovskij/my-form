<?php

/**
 * @category    My
 * @package     My_Form
 * @copyright   Copyright (c) 2020
 * @author      Roman Voikovskyi <rvojkovskij@gmail.com>
 */
declare(strict_types=1);

namespace My\Form\Api;

/**
 * Interface CountryResponseApiInterface
 * @package My\Form\Api
 */
interface CountryResponseApiInterface
{
    /**
     * @return array|null
     */
    public function getResponse(): ?array;
}
